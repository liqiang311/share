#include <iostream>
#include <vector>
#include <stack>

using namespace std;

#define max(a,b) (a>b?a:b)

int fun1(vector< vector<int> > &arr, int ii, int jj)
{
	vector< vector<int> > f;
	f.resize(arr.size());
	for (int i = 0; i < f.size(); i++)
	{
		f[i].resize(arr[i].size());
		for (int j = 0; j < arr[i].size(); j++)
		{
			f[i][j] = 0;
		}
	}
	f[ii][jj] = 0;
	stack<pair<int, int>> s;
	s.push(pair<int, int>(ii, jj));
	while (!s.empty())
	{
		pair<int, int>  p = s.top();
		s.pop();

		if (p.first - 1 >= 0)
		{
			f[p.first - 1][p.second] = max(f[p.first - 1][p.second], f[p.first][p.second] + 1);
			s.push(pair<int, int>(p.first - 1, p.second));
		}
		if (p.first + 1 < arr.size())
		{
			f[p.first + 1][p.second] = max(f[p.first + 1][p.second], f[p.first][p.second] + 1);
			s.push(pair<int, int>(p.first + 1, p.second));
		}
		if (p.second - 1 >= 0)
		{
			f[p.first][p.second - 1] = max(f[p.first][p.second - 1], f[p.first][p.second] + 1);
			s.push(pair<int, int>(p.first, p.second - 1));
		}
		if (p.second + 1 < arr.size())
		{
			f[p.first][p.second + 1] = max(f[p.first][p.second + 1], f[p.first][p.second] + 1);
			s.push(pair<int, int>(p.first, p.second + 1));
		}
	}

	int res = 0;
	for (int i = 0; i < f.size(); i++)
	{
		for (int j = 0; j < arr[i].size(); j++)
		{
			if (f[i][j] > res) res = f[i][j];
		}
	}
	return res;
}

int fun(vector< vector<int> > arr)
{
	int res = 0;
	for (int i = 0; i < arr.size(); i++)
	{
		for (int j = 0; j < arr[i].size(); j++)
		{
			int res1 = fun1(arr, i, j);
			if (res1 > res) res = res1;
		}
	}
	return res;
}

int main()
{
	int X=0, Y=0;
	vector<vector<int> > vec(X, vector<int>(Y, 0));
	while (cin >> X && cin >> Y)
	{
		for (int i = 0; i < X; i++)
			{
				for (int j = 0; j < Y; j++)
				{
					cin >> vec[i][j];
				}
			}
	}
	cout << fun(vec) << endl;
	return 0;
}