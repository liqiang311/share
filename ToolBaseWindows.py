# -*- coding:utf-8 -*-
# __author__ = 'GuanFengyu'
import Tkinter as tk
import os,subprocess
import tkFileDialog
import tkMessageBox as mb 

class BaseWindow():
    def __init__(self,window):
        self.window = window
        self.file_opt = options = {}
        options['filetypes'] = [('text files', '.txt'),('all files', '.*')] 
        #options['initialdir'] = 'C:\\' 
        self.center_window()
        

    def center_window(self,w = 600,h = 300):
        self.window.title('基于判定表生成测试案例')
        ws = self.window.winfo_screenwidth()
        hs = self.window.winfo_screenheight()
        self.window.geometry("%dx%d+%d+%d" % (w, h, (ws/2)-(w/2),(hs/2)-(h/2)))
        self.window.resizable(width = False,height=False )
        label = tk.Label(self.window, text="txt文件").grid(row = 0)
        self.filename = ''
        self.entry = tk.Entry(self.window)
        self.entry.grid(row =0,column = 1)
        button_liulan= tk.Button(self.window,text = '浏览',command = self.brower_path).grid(row =0,column = 2)
        button_case = tk.Button(self.window,text = '生成excel案例',command = self.read_txt).grid(row =1,column = 1)  
        self.window.mainloop()

    def brower_path(self):
        self.filename = tkFileDialog.askopenfilename(**self.file_opt)
        self.entry.insert(0,self.filename)
        
    def pict_case(self,path):

        # cmd = "pict " + path +"> " + os.path.join(os.path.dirname(path),'case_new.txt')
        cmd = "pict " + path
        p = subprocess.Popen(args = cmd,shell= True,stdout=subprocess.PIPE)
        case_txt = p.stdout.read()
        print(case_txt)

    def read_txt(self):
        if self.filename:
            f_open = open(self.filename,'r')
            self.pict_case(self.filename)
        else:
            mb.showinfo(title = '提示',message = '请输入文件路径！')
        

if __name__ == '__main__':
    window = tk.Tk()
    my_window = BaseWindow(window)

